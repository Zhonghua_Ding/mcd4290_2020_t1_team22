// Shared code needed by all three pages.

const mapboxToken =
        'pk.eyJ1IjoibWF5YmVkeWxhbiIsImEiOiJjazlqdG5uNXkwMHh3M2VwaGJ3cDZocHVrIn0.9GAEyGkIk-QIPwa657Xpig';
const mapboxCfg = { 
    container: 'map', // container id
    style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
    center: [145.0420733, -37.8770097], // starting position [lng, lat]
    zoom: 16 // starting zoom
};
mapboxgl.accessToken = mapboxToken;

// Prefix to use for Local Storage.  You may change this.
const APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
let savedRuns = [];

class Run {
    startLocation;
    destinationLocation;
    startTime;
    endTime;
    path = [];
    distance;
    duration;
    speed;

    constructor(startLocation, destinationLocation, startTime) {
        this.startLocation = startLocation;
        this.destinationLocation = destinationLocation;
        this.startTime = startTime;
    }

    completeRun(endTime) {
        this.endTime = endTime;

        this._calculateDistanceTravelled();// calculates distance travelled 
        this._calculateDurationTaken(); // calculates time taken 
        this._calculateAverageSpeed(); // calculates the speed 
    }

    addToPath(lngLat) {
        this.path.push(lngLat);
    }

    _calculateDistanceTravelled() {
        let distance = 0;
        
        for (let i = 1; i < this.path.length; i++) {
            distance += calculateDistance(this.path[i-1], this.path[i]);
        }
        
        this.distance = distance;
    }

    _calculateDurationTaken() {
        // divide by 1000 to get duration taken in seconds
        this.duration = ((this.endTime - this.startTime) / 1000).toFixed(2);
    }

    _calculateAverageSpeed() {
        // calculate the average speed in metres per second
        this.speed = (this.distance / this.duration).toFixed(2);
    }
}

//--------------------------------------------------------
// calculates distance from one point to another
function calculateDistance(lngLat1, lngLat2) {
    const earthRadius = 6371e3; // radius of earth in DistanceCalculation function
    
    const lat1Radians = toRadians(lngLat1.lat);
    const lat2Radians = toRadians(lngLat2.lat);
     
    const latRadians = toRadians(lngLat2.lat - lngLat1.lat);
    const longRadians = toRadians(lngLat2.lng - lngLat1.lng);
     
    const a = Math.sin(latRadians/2) * Math.sin(latRadians/2) +
              Math.sin(longRadians/2) * Math.sin(longRadians/2) *
              Math.cos(lat1Radians) * Math.cos(lat2Radians);
    
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    const d = earthRadius * c;
    
    return Math.round(d);
    
    function toRadians(val) {
        return val / 180.0 * Math.PI;
    }
}