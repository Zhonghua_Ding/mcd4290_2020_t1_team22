const map = new mapboxgl.Map(mapboxCfg); // map config set in shared.js

// this value was set higher (eg. 2000) when testing through web browser on laptop
const GEOLOCATION_ACCURACY = 20; // geolocation accuracy required to search a new destination (in metres)
const MIN_DESTINATION_DISTANCE = 60; // the min distance from the start required for the destination (in metres)
const MAX_DESTINATION_DISTANCE = 150; // the max distance from the start required for the destination (in metres)
const RUN_COMPLETION_THRESHOLD = 10; // the distance required for the run to be considering complete (in metres)

let currentRun;

let currentLngLat = new mapboxgl.LngLat(0, 0);
let destinationLngLat = new mapboxgl.LngLat(0, 0);

let startMarker = createMarker('green', 'start'); // when the user started run will label the green color marker 
let destinationMarker = createMarker('red', 'finish'); // the destination marker is red
let currentMarker = createMarker('blue', 'your location'); // when user is stationery the marker is blue 

let isDestinationSet = false;
let isRunning = false;
let isRunCompleted = false;

// set up new destination button
const newDestinationButton = document.getElementById('newDestination');
newDestinationButton.disabled = true; // disable the destination button 
newDestinationButton.onclick = newDestinationClicked; // when the user click the new destination button call newDestinationClicked function 

// set up start run button
const startRunButton = document.getElementById('startRun');
startRunButton.disabled = true; // disables the start run button
startRunButton.onclick = startRunClicked; // when user clicks start run button it will start tracking the run

// set up save run button
const saveRunButton = document.getElementById('saveRun');
saveRunButton.disabled = true; // disable the button 
saveRunButton.onclick = saveRunClicked; // calles saveRunClicked function when clicked 

const statusDisplay = document.getElementById('output');
const saveRunName = document.getElementById('runName');
saveRunName.disabled = true; // disables the textbox

// start watching location
const geolocationOptions = { enableHighAccuracy: true }; // forces the geolocation to find a more accurate position
navigator.geolocation.watchPosition(watchPositionSuccess, watchPositionError, geolocationOptions);
// this function will check the current situation 
function watchPositionSuccess(position) {
    // if the position has not changed then no need to do anything
    if (position.coords.longitude === currentLngLat.lng && position.coords.latitude === currentLngLat.lat) {
        return;
    }

    currentLngLat = new 
    mapboxgl.LngLat(position.coords.longitude,position.coords.latitude); // gets current location 
    currentMarker.setLngLat(currentLngLat); // adds the marker for current location 
    map.panTo(currentLngLat);
    
    updateDisplay(); // displays
    // when user starts running it will calulate distance left 
    if (isRunning) { 
        const distance = calculateDistance(currentLngLat, destinationLngLat);
        // checks if user is close to destination 
        if (distance < RUN_COMPLETION_THRESHOLD) {
            runCompleted();
        } else {
            const newLngLat = new mapboxgl.LngLat(currentLngLat.lng, currentLngLat.lat);
            currentRun.addToPath(newLngLat);
        }
    } else if (position.coords.accuracy < GEOLOCATION_ACCURACY) {
        newDestinationButton.disabled = false;
    }
}

function watchPositionError(error) {
    displayMessage('Error: ' + error.message);
}

function updateDisplay() {
    let status = '';
    
    if (isRunning) {
        const startTime = currentRun.startTime;
        status += '<p>Start time: ' + startTime.toLocaleDateString() + ' ' + startTime.toLocaleTimeString() + '</p>';
        status += '<p>Distance from start: ' + calculateDistance(currentRun.startLocation, currentLngLat) + 'm</p>';
    }
    
    if (isDestinationSet) {
        status += '<p>Distance to destination: ' + calculateDistance(currentLngLat, destinationLngLat) + 'm</p>';
    }
    
    if (isRunCompleted) {
        status += '<p>Distance travelled: ' + currentRun.distance + 'm</p>';
        status += '<p>Duration: ' + currentRun.duration + '</p>';
    }
    
    statusDisplay.innerHTML = status;
}

function runCompleted() {
    currentRun.completeRun(new Date());

    isDestinationSet = false;
    isRunning = false;
    isRunCompleted = true;
    
    newDestinationButton.disabled = false;
    startRunButton.disabled = true;
    saveRunName.disabled = false;
    saveRunButton.disabled = false;

    displayMessage('Run completed!');
    updateDisplay();
}

function randomDestination(lngLat) {
    let distance = 0;
    let randomLngLat;

    // check the distance generated does fall within the min and max
    while (distance < MIN_DESTINATION_DISTANCE || distance > MAX_DESTINATION_DISTANCE) {
        const randomLongitude = randomDistanceUnit() + lngLat.lng // random longitude coordinate
        const randomLatitude = randomDistanceUnit() + lngLat.lat // random latitude coordinate 

        randomLngLat = new mapboxgl.LngLat(randomLongitude, randomLatitude);

        distance = calculateDistance(currentLngLat, randomLngLat); // calculates the distance 
    }
    
    return randomLngLat;
}

function randomDistanceUnit() {
    let sign;
    let signValue = Math.floor(Math.random() * 2); // will return either a 0 or 1

    if (signValue === 0) {
        sign = -1; // set to negative
    } else {
        sign = 1; // set to positive
    }

    // distance unit has a min of 0.0004 and max of 0.001 (approximately a min of 60m and max of 150m)
    let distanceUnit = ((Math.random() * 0.0096) + 0.0004);

    return distanceUnit * sign;
}

function newDestinationClicked() {
    destinationLngLat = randomDestination(currentLngLat);
    
    destinationMarker // adding marker for destination 
        .setLngLat(destinationLngLat)
        .addTo(map);
    
    isDestinationSet = true;
    isRunning = false;
    isRunCompleted = false;
    
    startRunButton.disabled = false; // enabling startRunButton 
    saveRunButton.disabled = true;
    saveRunName.disabled = true;
    
    displayMessage('New destination set'); // displaying 
    updateDisplay(); // updating status 
}

function startRunClicked() {
    const startingLngLat = new mapboxgl.LngLat(currentLngLat.lng, currentLngLat.lat);
    const endingLngLat = new mapboxgl.LngLat(destinationLngLat.lng, destinationLngLat.lat);
    
    currentRun = new Run(startingLngLat, endingLngLat, new Date());

    startMarker
        .setLngLat(startingLngLat)
        .addTo(map);
    
    isRunning = true;

    newDestinationButton.disabled = true;
    startRunButton.disabled = true;
    saveRunName.disabled = true;
    saveRunButton.disabled = true;

    displayMessage('New run started');
    updateDisplay();
}

function saveRunClicked() {  
    if (!saveRunName.value) {
        displayMessage('Please choose a name before saving');
        return;
    }
    
    let runs = load('runs');
    if (!runs) runs = [];
    runs.push({ name: saveRunName.value, run: currentRun });
    save('runs', runs);
    
    location.href = 'index.html';
}
