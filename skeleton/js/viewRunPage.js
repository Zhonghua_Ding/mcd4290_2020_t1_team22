const map = new mapboxgl.Map(mapboxCfg); // map config set in shared.js

let runs = load('runs');
if (!runs) runs = [];

let selectedRun;

let startMarker = createMarker('green', 'start');
let destinationMarker = createMarker('red', 'finish');

// set up delete run button
const deleteRunButton = document.getElementById('deleteRun');
deleteRunButton.disabled = false;
deleteRunButton.onclick = deleteRunClicked;

const runIndex = localStorage.getItem(APP_PREFIX + '-selectedRun');
if (runIndex !== null)
{
    selectedRun = runs[runIndex];
    if (!selectedRun) location.href = 'index.html'; // return to list page if run doesnt exist

    // If a run index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the run being displayed.
    document.getElementById('headerBarTitle').textContent = selectedRun.name;
} else {
    location.href = 'index.html'; // return to list page if run index doesnt exist
}

let output = '';

output += '<p>Distance travelled: ' + selectedRun.run.distance + 'm</p>';
output += '<p>Duration taken: ' + selectedRun.run.duration + 's</p>';
output += '<p>Average speed: ' + selectedRun.run.speed + ' m/s</p>';

document.getElementById('output').innerHTML += output;

startMarker
    .setLngLat(selectedRun.run.startLocation)
    .addTo(map);

destinationMarker
    .setLngLat(selectedRun.run.destinationLocation)
    .addTo(map);
//creates marker for path the user took 
for (let i = 0; i < selectedRun.run.path.length; i++) {
    createMarker('blue', 'path-' + i)
        .setLngLat(selectedRun.run.path[i])
        .addTo(map);
}

map.panTo(selectedRun.run.startLocation);

function deleteRunClicked() {
    runs.splice(runIndex, 1);
    save('runs', runs);

    location.href = 'index.html'; // go back to the main page after deletion
}