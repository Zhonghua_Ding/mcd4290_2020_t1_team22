// Shared utility code needed by the code of all three pages.

// This function displays the given message String as a "toast" message at
// the bottom of the screen.  It will be displayed for 2 second, or if the
// number of milliseconds given by the timeout argument if specified.
function displayMessage(message, timeout)
{
    if (timeout === undefined)
    {
        // Timeout argument not specifed, use default.
        timeout = 2000;
    }

    if (typeof(message) == 'number')
    {
        // If argument is a number, convert to a string.
        message = message.toString();
    }

    if (typeof(message) != 'string')
    {
        console.log("displayMessage: Argument is not a string.");
        return;
    }

    if (message.length == 0)
    {
        console.log("displayMessage: Given an empty string.");
        return;
    }

    var snackbarContainer = document.getElementById('toast');
    var data = {
        message: message,
        timeout: timeout
    };
    if (snackbarContainer && snackbarContainer.hasOwnProperty("MaterialSnackbar"))
    {
        snackbarContainer.MaterialSnackbar.showSnackbar(data);
    }
}


function createMarker(color, text) {
    console.log('Creating new marker with text: ' + text);
    
    const popup = new mapboxgl.Popup({ offset: 25 })
        .setText(text);
    
    const marker = new mapboxgl.Marker({ color: color })
        .setPopup(popup)
        .setLngLat(new mapboxgl.LngLat(0,0))
        .addTo(map);
    
    return marker;
}

// coverts in JSON because its an object and then saves
function save(key, value) {
    localStorage.setItem(APP_PREFIX + '.' + key, JSON.stringify(value));
}

// gets from local storage and parse the Json
function load(key) {
    return JSON.parse(localStorage.getItem(APP_PREFIX + '.' + key));
}