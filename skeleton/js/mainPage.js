// Code for the main app page (Past Runs list).

let runs = load('runs');
if (!runs) runs = [];

document.getElementById('runsList').innerHTML = '';

for (let i = 0; i < runs.length; i++) {
    const startTime = new Date(runs[i].run.startTime);
    const output = `<li class="mdl-list__item mdl-list__item--two-line" onclick="viewRun(` + i + `);">
            <span class="mdl-list__item-primary-content">
              <span>` + runs[i].name + `</span>
              <span class="mdl-list__item-sub-title">
                Start time: ` + startTime.toLocaleDateString() + ' ' + startTime.toLocaleTimeString() + `
              </span>
            </span>
          </li>`;

    document.getElementById('runsList').innerHTML += output;
}

function viewRun(runIndex)
{
    // Save the desired run to local storage so it can be accessed from View Run page.
    localStorage.setItem(APP_PREFIX + '-selectedRun', runIndex);
    // ... and load the View Run page.
    location.href = 'viewRun.html';
}